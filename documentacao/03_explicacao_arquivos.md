#### Explicação de cada arquivo

`README.ME`

Define o conteúdo do documento que você está lendo agora.

`.gitignore`

Quais arquivos/pastas vão ser ignorados, ou seja, não vai para o repositório git.

`requirements.txt`

Dependências do ambiente virtual que vai ser usado nesse projeto. Ou seja os pacotes do python que vão estar disponíveis para serem importados e usados.

`run_virtual_enviroment.ps1`

Script power shell (só pode ser usado do windows), para facilitar a execução do ambiente virtual.

`pyproject.toml`

Colocado para configurar o fomatador de código `black`

> PEP 518 defines pyproject.toml as a configuration file to store build system requirements for Python projects. With the help of tools like Poetry, Flit, or Hatch it can fully replace the need for setup.py and setup.cfg files.
