#### Criar README.md, arquivo que representa o conteúdo que está sendo lido agora

- Instalar pandoc

  > https://pandoc.org/getting-started.html

- Juntar todos os arquivos dentro da pasta `documentacao` em um único arquivo chamado `README.md`

```
compilar_documentacao.ps1
```
