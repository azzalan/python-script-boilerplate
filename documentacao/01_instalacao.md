#### Instalando - primeiros passos

- Instalar python no windows
- Instalar git no windows
- Configurar sua autenticação no git por ssh
- Abrir powershell como administrador

```
Tecla do windows
Escreva "powershell"
Click na opção "Executar como administrador"

```

- Instalar pacote que permite usar gerenciar pacotes usando ambientes virtuais com maior facilidade

```
pip install pipenv

```

- Navegar uma pasta pai onde você deseja guardar a pasta do projeto projeto, usaremos `C:/_code` como referência para o resto da documentação.

```
cd C:/_code
```

- Baixar arquivos do projeto

```
git clone git@gitlab.com:azzalan/python-script-boilerplate.git
```

- Navegar para a pasta que acabou de ser criada

```
cd python-script-boilerplate
```

- Instalar os pacotes básicos do projeto.

```
pipenv install
```
