#### Instalando - primeiros passos

- Instalar python no windows
- Instalar git no windows
- Configurar sua autenticação no git por ssh
- Abrir powershell como administrador

<!-- -->

    Tecla do windows
    Escreva "powershell"
    Click na opção "Executar como administrador"

- Instalar pacote que permite usar gerenciar pacotes usando ambientes
  virtuais com maior facilidade

<!-- -->

    pip install pipenv

- Navegar uma pasta pai onde você deseja guardar a pasta do projeto
  projeto, usaremos `C:/_code` como referência para o resto da
  documentação.

<!-- -->

    cd C:/_code

- Baixar arquivos do projeto

<!-- -->

    git clone git@gitlab.com:azzalan/python-script-boilerplate.git

- Navegar para a pasta que acabou de ser criada

<!-- -->

    cd python-script-boilerplate

- Instalar os pacotes básicos do projeto.

<!-- -->

    pipenv install

#### Explicação de cada arquivo

`README.ME`

Define o conteúdo do documento que você está lendo agora.

`.gitignore`

Quais arquivos/pastas vão ser ignorados, ou seja, não vai para o
repositório git.

`requirements.txt`

Dependências do ambiente virtual que vai ser usado nesse projeto. Ou
seja os pacotes do python que vão estar disponíveis para serem
importados e usados.

`run_virtual_enviroment.ps1`

Script power shell (só pode ser usado do windows), para facilitar a
execução do ambiente virtual.

`pyproject.toml`

Colocado para configurar o fomatador de código `black`

> PEP 518 defines pyproject.toml as a configuration file to store build
> system requirements for Python projects. With the help of tools like
> Poetry, Flit, or Hatch it can fully replace the need for setup.py and
> setup.cfg files.

#### Criar README.md, arquivo que representa o conteúdo que está sendo lido agora

- Instalar pandoc

  > https://pandoc.org/getting-started.html

- Juntar todos os arquivos dentro da pasta `documentacao` em um único
  arquivo chamado `README.md`

<!-- -->

    compilar_documentacao.ps1
